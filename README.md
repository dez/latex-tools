My miscellaneous LaTeX packages
===============================

This repository hosts various small LaTeX packages that I maintain for may
personal use but may be useful to other people.

Packages
--------

- `ebutf8.sty` enables UTF8 input in text and math independently of the TeX
  engine (builds upon `inputenc`, `unicode-math` etc and includes the
  declaratin of many Unicode symbols).

- `paranum.sty` provides numbered paragraphs and integrates with `amsthm`
  theorem-like environments

- `ebmath.sty` defines various math constructs that I often need, including
  - helpers to make macros that include variable-size delimiters,
  - syntax definitions in BNF style,
  - facilities for using symbols in text (as in "λ-calculus").

- `exo.sty` provides format for exercises and answers.

Custom document classes
-----------------------

- `ebarticle.cls`, `ebarticlef.cls` is a generic format for articles (the `f`
  one is in French).
- `ebcoursef.cls` is a generic format for course notes (in French).
- `ebdraft.sty` is a handful of settings for draft versions of documents.
- `ebsheet.cls`, `ebsheetf.cls` is a generic format for exercise sheets.
