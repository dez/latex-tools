% vim: syntax=tex

% Classe de documents pour notes de cours en français.
%
% Forme générale:
%
%   \documentclass{cours}
%   \title{titre du cours}
%   \date{comme d'hab}
%   \begin{document}
%   \maketitle
%     ...
%   \end{document}
%
% Environnement de type théorème définis:
%   definition, example, theorem, proposition, lemma
%
% Format des exercices: voir exo.sty.
% Par défaut, les indications sont formatées à l'envers et les corrections
% sont reportées en fin de document.
%
% Les fontes utilisées sont:
% - Charter pour le texte,
% - Euler pour les maths,
% - Bera Mono pour le texte à chasse fixe.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ebcoursef}[2010/11/29 EB's format for course notes in French]

% Options

\newif\ifeb@paranum
\DeclareOption{paranum}{\eb@paranumtrue}

% On se base sur la classe book pour avoir les chapitres, sections, etc.

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClassWithOptions{book}

% Réglages pour le français

\RequirePackage[frenchb]{babel}
%\FrenchItemizeSpacingfalse

% Mise en page

\RequirePackage[a4paper,width=15cm,height=22cm]{geometry}

% \RequirePackage{charter,euler}
% \RequirePackage[scaled]{beramono}

% Format du titre

\RequirePackage{ifthen}

\newcommand\header[1]{\gdef\@header{#1}}
\def\@header{}

\newcommand\course[1]{\gdef\@course{#1}}
\def\@course{}

\newenvironment{coverpage}{%
  \parindent=0pt\relax
  \begin{titlepage}
  \ifthenelse{\equal{\@header}{}}{}{%
    \begin{flushleft}
      \@header
    \end{flushleft}}%
  \begin{center}
    \null\vfill
    \vrule height 1pt width .5\textwidth
    \vfill
    \Huge \bfseries \@title \par
    \vskip 1cm
    \Large \@author \par \vskip 1em \@date \par
    \vfill
    \vrule height 1pt width .5\textwidth
    \vfill
  \end{center}
}{
  \end{titlepage}
}

\renewcommand\maketitle{%
  \begin{coverpage}%
  \end{coverpage}}

% Format des sections et en-têtes

\RequirePackage[medium,pagestyles]{titlesec}

\renewpagestyle{plain}{}

\setmarks{chapter}{section}

\newpagestyle{main}[]{%
  \sethead%
    [\llap{\bfseries\thepage\quad}%
      \textit{\ifthechapter{Chapitre \thechapter\ -- }{}\chaptertitle}][][]%
    {}{}{\textit{\ifthechapter{\thesection. \sectiontitle}{\chaptertitle}}%
      \rlap{\quad\bfseries\thepage}}}

\pagestyle{main}


% Quelques packages utiles

\RequirePackage{url}
\RequirePackage{amssymb,amsmath,stmaryrd}
\RequirePackage{array}
\RequirePackage[unicode,colorlinks]{hyperref}

\AtBeginDocument{%
  \hypersetup{%
    pdftitle=\@title,
    pdfauthor=\@author }}


% Questions de codage, selon qu'on est sous XeTeX ou pas

\expandafter\ifx\csname XeTeXrevision\endcsname\relax
  \RequirePackage[T1]{fontenc}
\else
  \RequirePackage{xltxtra}
\fi


% Définitions, théorèmes, etc.

\RequirePackage{amsthm}

\ifeb@paranum

\RequirePackage{paranum}

\newcounter{paranum}[chapter]
\renewcommand\theparanum{\thechapter.\arabic{paranum}}

\theoremstyle{definition}
\newtheorem{definition}[paranum]{D\'efinition}
\theoremstyle{remark}
\newtheorem{example}[paranum]{Exemple}
\theoremstyle{plain}
\newtheorem{theorem}[paranum]{Th\'eor\`eme}
\newtheorem{proposition}[paranum]{Proposition}
\newtheorem{lemma}[paranum]{Lemme}
\newtheorem{corollary}[paranum]{Corollaire}

\newenvironment{remark}{\paranum}{}

\else

\theoremstyle{definition}
\newtheorem{definition}{D\'efinition}[chapter]
\newtheorem{example}{Exemple}[chapter]
\newtheorem{remark}{Remarque}[chapter]
\theoremstyle{plain}
\newtheorem{theorem}{Th\'eor\`eme}[chapter]
\newtheorem{proposition}{Proposition}[chapter]
\newtheorem{lemma}{Lemme}[chapter]
\newtheorem{corollary}{Corollaire}[chapter]

\fi

% Exercices et solutions

\RequirePackage{graphicx,xcolor}
\RequirePackage[hints=reversed,answers=deferred]{exo}
\ExoConfig{hintsetup=\small\color{gray}}

\@addtoreset{exercise}{chapter}
\renewcommand\theexercise{\thechapter.\arabic{exercise}}
\renewcommand\exercisename{Exercice}
\renewcommand\hintname{Indication}
\renewcommand\answername{Correction}
