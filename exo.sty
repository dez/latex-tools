% vim: syntax=tex

% A package for typesetting exercises and answers.
%
% To load the package, say:
%
%   \usepackage[options]{exo}
%
% The options can be changed later in the preamble by saying:
%
%   \ExoConfig{options}
%
% The options are key/value pairs of the following forms:
%
%    spacing=<mode> : set predefined spacing rules
%       text - for exercises inserted in a text
%       sheet - for exercise sheets
%    inline=<bool> : sets if the 'exercise' header is on the same line as the
%                    beginning of the exercise or not (default: yes)
%    hints=<style> : set the style for hints, among:
%       ignored  - hints are not displayed
%       inline - as plain text, with parentheses and 'Hint' written before
%       reversed - upside-down
%    hintsetup=<commands> : additional commands to set up hints
%    answers=<style> : set the style for answers, among:
%       ignored - answers are not written
%       inline - as a paragraph introduced by the word 'Answer'
%       deferred - at the end of the document, where \InsertAnswers is put
%    answersetup=<commands> : additional commands to set up answers
%
% The default settings are 'hints=inline' and 'answers=inline'. With
% 'hints=reversed', you should load the 'graphicx' and 'color' packages (or
% anything else that provides \rotatebox and \color) yourself. With
% 'answers=deferred', \InsertAnswers must be put after all answers in the
% document. This mode (currently) interacts badly with verbatim environments.
%
% Format of exercises:
%
%   \begin{exercise}[optional title]
%     ...
%     \begin{questions}
%     \item First question...
%     \item Second question...
%       ...
%     \end{questions}
%     ...
%   \end{exercise}
%
% The "questions" environment is similar to the "enumerate" environments, with
% a slightly different format. Most importantly, successive environments are
% numbered continuously, instead of starting back at 1 each time, so that
% additional text can be written between successive series of questions. These
% environments can be nested to make sub-questions, of course each question
% resets the counter for sub-questions. 3 levels are defined by default, using
% counters "question", "questiion" and "questiiion". More levels can be
% added by defining counters "questivon", "questvon", "questvion" and so on.
%
%   \begin{hint}
%     ...
%   \end{hint}
%
% Writes a hint for the current exercise or question, in the current style.
%
%   \begin{answer}
%     ...
%   \end{answer}
%
% Writes the answer for the current exercise or question, in the current
% style.
%
%   \ifanswers ... \fi
%
% A conditional that is true when answers are actually written (i.e. in styles
% 'inline' and 'deferred').

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{exo}[2010/11/08 exercises and answers]

%%%  Language-dependent things

\providecommand\exercisename{Exercise}
\providecommand\hintname{Hint}
\providecommand\answername{Answer}


%%%  Configuration

\def\exo@spacing@text{%
  \def\exo@space@before{%
    \medbreak}}
\def\exo@spacing@sheet{%
  \def\exo@space@before{%
    \penalty-100\vskip 3ex plus 8em minus 1ex\relax}}

\RequirePackage{xkeyval}

\define@boolkey{exo.sty}[exo@head@]{inline}[true]{}
\define@key{exo.sty}{spacing}{\csname exo@spacing@#1\endcsname}
\define@key{exo.sty}{hints}{\def\exo@hint@style{#1}}
\define@key{exo.sty}{answers}{%
  \def\exo@answer@style{#1}%
  \csname exo@answer@config@#1\endcsname}
\define@key{exo.sty}{hintsetup}{\def\exo@hint@setup{#1}}
\define@key{exo.sty}{answersetup}{\def\exo@answer@setup{#1}}
\define@key{exo.sty}{columns}{%
  \def\exo@begin@columns{\begin{multicols}{#1}}%
  \def\exo@end@columns{\end{multicols}}}
\define@key{exo.sty}{answernocolumns}[]{%
  \ifanswers
  \let\exo@begin@columns\relax
  \let\exo@end@columns\relax
  \fi}

\newif\ifanswers

\exo@head@inlinetrue
\exo@spacing@text
\def\exo@hint@style{inline}
\def\exo@answer@style{inline}
\answerstrue
\def\exo@hint@setup{}
\def\exo@answer@setup{}

\let\exo@answer@config@ignored=\answersfalse
\let\exo@answer@config@inline=\answerstrue
\let\exo@answer@config@deferred=\answerstrue

\let\exo@begin@columns\relax
\let\exo@end@columns\relax

\ProcessOptionsX\relax

\def\ExoConfig{\setkeys{exo.sty}}


%%%  Format of exercises

\RequirePackage{ifthen}

\newcounter{exercise}

\newenvironment{exercise}[1][]{%
  \par \exo@space@before \noindent
  \refstepcounter{exercise}%
  \textbf{\exercisename\ \theexercise}%
  \ifthenelse{\equal{#1}{}}{}{ -- {\itshape #1}}%
  \ifexo@head@inline
    \textbf{.}
  \else
    \par\penalty1000%
    \list{}{%
      \topsep=0pt%
      \partopsep=0pt%
      \leftmargin=0pt%
      \parindent=0pt%
      \parskip=0pt}%
    \item
  \fi
  \exo@exercise@begin@hook
}{%
  \ifexo@head@inline\else
    \endlist
  \fi
  \exo@exercise@end@hook
}

% The hooks are used in the 'deferred' style of answers to put required
% information in the answer file.

\let\exo@exercise@begin@hook\relax
\let\exo@exercise@end@hook\relax


%%%  Format of questions

\newcounter{question}[exercise]
\newcounter{questiion}[question]
\newcounter{questiiion}[questiion]

\renewcommand\thequestiion{\alph{questiion}}
\renewcommand\thequestiiion{\roman{questiiion}}

\newcount\exo@quest@level
\exo@quest@level=0

\newenvironment{questions}[1][]{%
  \ExoConfig{#1}%
  \exo@questions@begin@hook
  \advance\exo@quest@level1%
  \edef\exo@quest@c{quest\romannumeral\exo@quest@level on}%
  \exo@begin@columns
  \list{%
    \csname the\exo@quest@c\endcsname.\exo@question@hook
  }{%
    \@nmbrlisttrue\edef\@listctr{\exo@quest@c}%
    \def\makelabel##1{\bfseries##1}%
    \leftmargin=17pt%
    \labelwidth=\leftmargin \advance\labelwidth-1ex%
    \labelsep=1ex%
  }%
}{%
  \endlist
  \exo@end@columns
  \exo@questions@end@hook
}

\let\exo@questions@begin@hook\relax
\let\exo@questions@end@hook\relax
\let\exo@question@hook\relax


%%%  Moving around the contents of an environment

% \exo@collect@ reads all tokens until the end of the environment and
% accumulates them in the register \exo@toks. The name of the environment must
% be put in \exo@environment. The code for the end of the environment is
% called afterwards.

\newtoks\exo@toks

\def\exo@collect{%
  \edef\exo@environment{\@currenvir}%
  \exo@toks{}%
  \exo@collect@}

\long\def\exo@collect@#1\end#2{%
  \expandafter\exo@toks\expandafter{\the\exo@toks#1}%
  \def\exo@test{#2}%
  \ifx\exo@test\exo@environment
    \expandafter\exo@process
  \else
    \expandafter\exo@toks\expandafter{\the\exo@toks\end{#2}}%
    \expandafter\exo@collect@
  \fi}

\def\exo@process{%
  \expandafter\end\expandafter{\exo@environment}}


%%%  Formats for hints

\newsavebox\exo@hint@box

\newenvironment{hint}{%
  \exo@collect
}{%
  \expandafter\expandafter\csname exo@process@hint@\exo@hint@style\endcsname
  \expandafter{\the\exo@toks}%
}

\def\exo@process@hint@ignored#1{}

\def\exo@process@hint@inline#1{%
  {\exo@hint@setup(\hintname: #1\@bsphack)}}

\def\exo@process@hint@footnote#1{%
  \footnote{#1}}

\def\exo@process@hint@reversed#1{%
  \par
  \rotatebox{180}{%
    \begin{minipage}{\linewidth}%
      \exo@hint@setup#1%
    \end{minipage}%
  }%
}


%%%  Formats for answers

\AtBeginDocument{%
  \csname exo@answer@prepare@\exo@answer@style\endcsname}

\newenvironment{answer}{%
  \csname exo@answer@begin@\exo@answer@style\endcsname
}{%
  \csname exo@answer@end@\exo@answer@style\endcsname
}

%%  Ignored

\def\exo@answer@begin@ignored{%
  \exo@collect}
\def\exo@answer@end@ignored{}

%%  Inline

\def\exo@answer@begin@inline{%
  \par\exo@answer@setup
  \list{\kern\labelsep\textbf{\answername}}{%
    \leftmargin=0mm%
    \labelwidth=0mm%
    \parsep=0mm%
  }%
  \item~}
\def\exo@answer@end@inline{%
  \endlist}

%%  Deferred

\RequirePackage{verbatim}

% This one is a bit complicated because we want to produce reasonable ouput
% for the deferred answers. This includes putting the question numbers before
% the answers, but we want to put the numbers only for the questions where
% there are answers.

% Remembering if the exercise number was written out

\let\exo@answer@exercise@ifwritten\iftrue
\def\exo@answer@exercise@notwritten{%
  \global\let\exo@answer@exercise@ifwritten\iffalse}
\def\exo@answer@exercise@written{%
  \global\let\exo@answer@exercise@ifwritten\iftrue}

% Remembering the full question number, and if it was written out

\let\exo@answer@question@ifwritten\iffalse
\def\exo@answer@question@notwritten{%
  \global\let\exo@answer@question@ifwritten\iffalse}
\def\exo@answer@question@written{%
  \global\let\exo@answer@question@ifwritten\iftrue}

\def\exo@answer@prepare@deferred{%
  \newwrite\exo@answer@file
  \immediate\openout\exo@answer@file=\jobname.ans
  \let\exo@exercise@begin@hook\exo@answer@exercise@notwritten
  \def\exo@exercise@end@hook{%
    \exo@answer@exercise@ifwritten
      \immediate\write\exo@answer@file{%
      \noexpand\end{exerciseanswer}}%
    \fi}
  \def\exo@questions@begin@hook{%
    \ifnum\exo@quest@level=0%
      \edef\exo@question@prefix{}%
    \else
      \edef\exo@question@prefix{%
        \exo@question@prefix\csname the\exo@quest@c\endcsname.}%
    \fi}%
  \let\exo@question@hook\exo@answer@question@notwritten
}

\def\exo@answer@insert@deferred{%
  \immediate\closeout\exo@answer@file
  \input{\jobname.ans}%
}

% The code for writing the contents of the environments to the output file is
% taken from the documentation of the 'verbatim' package.

\def\exo@answer@begin@deferred{%
% write out the exercise and question numbers if they have changed
  \exo@answer@exercise@ifwritten\else
    \immediate\write\exo@answer@file{%
      \noexpand\begin{exerciseanswer}{\theexercise}}%
    \exo@answer@exercise@written
  \fi
  \exo@answer@question@ifwritten
    \immediate\write\exo@answer@file{\noexpand\par}%
  \else
    \ifnum\exo@quest@level=0\else
      \immediate\write\exo@answer@file{%
        \noexpand\questionanswer{%
          \exo@question@prefix\csname the\exo@quest@c\endcsname}}%
    \fi
    \exo@answer@question@written
  \fi
% start dumping the contents of the environment
  \@bsphack
  \let\do\@makeother\dospecials
  \catcode`\^^M\active
  \def\verbatim@processline{%
    \immediate\write\exo@answer@file{\the\verbatim@line}}%
  \verbatim@start}

\def\exo@answer@end@deferred{%
  \@esphack}


%%%  Format of deferred answers

\def\InsertAnswers{%
  \csname exo@answer@insert@\exo@answer@style\endcsname}

\newenvironment{exerciseanswer}[1]{%
  \ifhmode \par \fi \medskip
  \noindent\textbf{\exercisename\ #1}%
  \par
}{%
}

\newcommand\questionanswer[1]{%
  \par \noindent\textbf{#1. }%
  \ignorespaces}
