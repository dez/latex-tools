% vim: syntax=tex

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ebarticle}[2010/09/13 EB's article format]

\LoadClass{article}

% General format

\RequirePackage[a4paper]{geometry}

\expandafter\ifx\csname XeTeXrevision\endcsname\relax
  \RequirePackage[T1]{fontenc}
\fi

\RequirePackage{microtype}
\RequirePackage{lmodern}
\RequirePackage[unicode]{hyperref}

\AtBeginDocument{\hypersetup{%
  bookmarks=true, unicode=true, pdfborder={0 0 0},
  pdftitle={\@title}, pdfauthor={\@pdfauthor}}}

% Format of the title

\def\@institute{}

\newcommand\institute[1]{%
  \gdef\@institute{%
    \vskip .3em
    {#1}}}

\let\affiliation=\institute
\newcommand\email[1]{}

\renewcommand\@maketitle{%
  \newpage
  \null
  \vskip 2em%
  \begin{flushleft}%
  \let \footnote \thanks
    {\LARGE \@title \par}%
    \vskip 1em%
    {\large
      \lineskip .5em%
      \def\and{\unskip,\quad}%
      \@author
      \@ifundefined{hypersetup}{}{\hypersetup{pdfauthor={\@author}}}%
      \par}%
    \@institute%
    \vskip .7em%
    {\large \@date}%
  \end{flushleft}%
  \par
  \vskip 1.5em}

% Format of the abstract

\renewenvironment{abstract}{%
  \small
  \list{}{%
    \leftmargin=\z@
    \rightmargin=3em
  }%
  \item\relax
  \textbf{\abstractname.}
  \ignorespaces
}{%
  \endlist
  \bigskip
}

% Table of contents

\renewcommand\tableofcontents{%
  \bigskip
  \begingroup\small
  \hsize=.7\hsize
  \@starttoc{toc}%
  \endgroup
  \bigbreak
}
\renewcommand*\l@section[2]{%
  \@dottedtocline{1}{1ex}{1.5em}{\bfseries#1}{\bfseries#2}}
\renewcommand*\l@subsection[2]{%
  \@dottedtocline{2}{0em}{2em}{\kern1.5em\relax#1}{#2}}

% Theorem environments

\RequirePackage{amsthm}
\RequirePackage{paranum}

\newcounter{paranum}

\theoremstyle{definition}
\newtheorem{definition}[paranum]{\definitionname}
\newtheorem{notation}[paranum]{\notationname}
\theoremstyle{remark}
\newtheorem{example}[paranum]{\examplename}
\theoremstyle{plain}
\newtheorem{theorem}[paranum]{\theoremname}
\newtheorem{proposition}[paranum]{\propositionname}
\newtheorem{lemma}[paranum]{\lemmaname}
\newtheorem{corollary}[paranum]{\corollaryname}

\newenvironment{remark}{\paranum}{}

\providecommand\definitionname{Definition}
\providecommand\notationname{Notation}
\providecommand\examplename{Example}
\providecommand\remarkname{Remark}
\providecommand\theoremname{Theorem}
\providecommand\propositionname{Proposition}
\providecommand\lemmaname{Lemma}
\providecommand\corollaryname{Corollary}
